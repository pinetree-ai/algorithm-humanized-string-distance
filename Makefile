CXX := g++
EXE := hsd
# DAT := -D__DATA__
FLAGS := -Ofast -std=c++17 -fopenmp $(DAT)
INC := -I./include

LIB := src/transitions.cxx \
		src/options.cxx \
		src/hsd.cxx \
		src/library.cxx

SRC := $(LIB) src/main.cxx \
		src/help.cxx
	   	

OBJ := $(SRC:.cxx=.obj)

# Make rules
all: $(EXE)

$(EXE): $(OBJ)
	$(CXX) $^ -o $@ $(INC) $(FLAGS)

%.obj: %.cxx
	$(CXX) $(INC) $(FLAGS) -c $< -o $@

lib: $(LIB)
	$(CXX) $(LIB) -o libhsd.so -shared $(INC) $(FLAGS)

clean:
	rm -f $(OBJ)