# Humanized String Distance Algorithm

This project is created and maintained by [Inventives, Inc.](https://inventives.ai), and is licensed under the [Creative Commons Attribution-NonCommercial 4.0 International License](https://creativecommons.org/licenses/by-nc/4.0/legalcode).

## About

The *Humanized String Distance* (HSD) algorithm is based on a modified dynamic-time-warping solution to compare two strings. The HSD algorithm accounts for closeness of characters based on handwritten and/or extracted (OCR) text. For example, the **i** character looks similar to **j** and handwriting recognition systems may easily mistake them for each other based on the writing style. Handwritten or extracted characters like **B** and **8** are easily confused, similar to **S** and **5**, **.** and **,** and many more. The HSD algorithm is a lot more tolerant of these and improves the performance of string distance calculation to match extracted text to a known set of values.

The HSD algorithm takes in the extracted text, and expected/desired text as arguments, and provides a modified string distance score.

The expected/desired string may include lower case alphabets, numbers, and the following special characters:
 - Space ( )
 - Period (.)
 - Comma (,)
 - Hyphen (-)

A transition map is presented in the file `transitions.csv` which map possible extracted characters (rows) to desired characters (columns). The corresponded cell for each row-column represents a score on the scale 0 to 1 representing how similar the characters are. For instance, **q** and **v** are rarely confused, so they have a low score (0), but **b** and **h** may be confused easily, giving them a higher score (0.3). If the row and column characters are the same, then the cell value will be 1 representing an exact match.

You may change the `transitions.csv` file as needed to add new characters or transitions as needed - but please share it with the rest of us! We did share our algorithm with you...

## Build

```
make
```

## Usage

```
hsd {EXTRACTED} [-s {DESIRED} | -o {OPTS_FILE}] [OPTIONS]
```

- **EXTRACTED** - extracted (handwritten) string
- **DESIRED** - desired or expected string to match against
- **OPTS_FILE** - file path to a CSV file with a list of possible options to match against

### Options

- **-s** - match against a single desired string
- **-o** - match against a list of options and select N best values
- **-k** - next argument is a string key - options are cells under the key in the options file
- **-t** - path to a custom transitions CSV file
- **-p** - print total computation time for profiling

### Examples

Get the string distance between "Pem5v1vanja" and "Pennsylvania".
```
hsd Pem5v1vanja -s pennsylvania
```

*NOTE* that we we passed the desired text in all lower case.

We have an options file `options.csv` in the current working directory with lists of possible options for cities, states, and zipcodes. The headers for each of the 3 columns are **city**, **state**, and **zip**.

Get the best possible match for the extracted text for city: "N3VV V0RK"
```
hsd "N3VV V0RK" -o ./options.csv -k city
```