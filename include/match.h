#ifndef _MATCH_H_
#define _MATCH_H_

#include <string>
using namespace std;

struct Match {
    string value;
    double distance;
};

#endif