#include "help.h"

#include <iostream>

using namespace std;

void help()
{
    cout << "Usage: hsd {extracted} [-s {desired} | -o {options file path}] [-t {transitions file path}] [-k {key}] [-p]" << endl;
    cout << "  extracted: extracted handwritten text" << endl;
    cout << "  desired: desired or expected text to match with" << endl;
    cout << "  options file path: a csv file with a list of possible options to match against." << endl;
    cout << "       transitions file path: relative or absolute path to transitions file." << endl << endl;
    cout << "Options:" << endl;
    cout << "  -s: calculate HSD between the extracted text and a single string" << endl;
    cout << "  -o: calculate HSD between the extracted text and a list of options" << endl;
    cout << "  -t: provide a path to a custom transitions file." << endl;
    cout << "  -k: compare against cells under the provided `key` in the options list." << endl;
    cout << "  -p: Print computation time." << endl << endl;
    cout << "Use the -s flag to test against a single string, or -o to test against multiple options provided in a csv file." << endl << endl;
    exit(-1);
}