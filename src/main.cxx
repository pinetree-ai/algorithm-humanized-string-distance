#include "library.h"
#include "help.h"

#include <iostream>
#include <chrono>
#include <cstring>

using namespace std;

int main(int argc, char **argv)
{
    string mode = "string";
    string extracted, desired = "";
    string optionsFilepath = "", optionsKey = "";
    string transitionsFilepath = "";
    int numResults = 1;
    bool profile = false;

    // Ensure input arguments are present, else show the help message
    if (argc < 4) {
        cerr << "Invalid usage." << endl;
        help();
    }

    // Read input arguments
    extracted = argv[1];
    if (strcmp(argv[2], "-o") == 0) {
        mode = "options";
        optionsFilepath = argv[3];
    }
    else desired = argv[3];

    // Remaining arguments are passed with flags
    string flag = "";
    for (int i=4; i<argc; i++) {
        if (flag == "") {
            if (strcmp(argv[i], "-p") == 0) profile = true;
            else flag = argv[i];
        }
        
        else {
            if (flag == "-s") desired = argv[i];
            else if (flag == "-o") optionsFilepath = argv[i];
            else if (flag == "-k") optionsKey = argv[i];
            else if (flag == "-t") transitionsFilepath = argv[i];
            else if (flag == "-n") numResults = stod(argv[i]);
            
            flag = ""; // Reset flag
        }
    }

    auto start = chrono::high_resolution_clock::now();

    // Calculate string distance between extracted and desired strings
    // Find best match in a list of provided options
    if (mode == "options") {
        // Ensure we have an options filepath
        if (optionsFilepath == "") {
            cerr << "Missing options file path. See usage." << endl << endl;
            help();
        }

        // Calculate HSD for each option
        vector<Match> matches = findBestMatchesFromFile(extracted, optionsFilepath, optionsKey, numResults, transitionsFilepath);

        // Print best matches along with scores
        for (int i=0; i<matches.size(); i++) {
            if (i>=numResults) break; // Only top n matches
            cout << matches.at(i).value << "\t" << matches.at(i).distance << endl;            
        }
    }

    // Calculate the HSD between 2 strings
    else {
        if (desired == "") {
            cerr << "Missing desired text to match against. See usage." << endl << endl;
            help();
        }

        cout << calculateDistanceBetween(extracted, desired, transitionsFilepath) << endl;
    }

    if (profile) {
        auto stop = chrono::high_resolution_clock::now();
        auto dt = chrono::duration_cast<chrono::microseconds>(stop-start);
        cout << endl << "Computation time: " << dt.count() << "us (microseconds)" << endl;
    }
}